#df_Groups=df.groupby(df['HitterId']).sum()
#df_Groups

import pandas as pd
import numpy as np
from pandas import DataFrame


df = pd.read_csv("E:/InsideEdge/Repository/python_hiring_test/python_hiring_test/data/raw/pitchdata.csv")
df.columns = ["GameId",'PitcherId','HitterId','PitcherSide','HitterSide','PrimaryEvent','PitcherTeamId','HitterTeamId','PA','AB','H','DOUBLE','TRIPLE','HR','TB','BB','SF','HBP']
df.head()

df_Avg = df.filter(["GameId",'PitcherId','HitterId','PitcherSide','HitterSide','PrimaryEvent','PitcherTeamId','HitterTeamId','PA','AB','H','DOUBLE','TRIPLE','HR','TB','BB','SF','HBP'],axis=1)
df_Avg = df.drop(['PitcherId','HitterId','PitcherSide','HitterSide','PrimaryEvent','PitcherTeamId','HitterTeamId','AB','H','DOUBLE','TRIPLE','HR','TB','BB','SF','HBP'],axis=1)
df_Avg['GameId'] = df.GameId
df_Avg['PA'] = df.PA
df_Avg['Value']= pd.DataFrame(df.H / df.AB)
df_Avg['Stat'] = 'AVG'
df_Avg.columns = ['GameID',"PA","Value","Stat"]
df_Avg.round(3)

df_OBP = df.filter(["GameId",'PitcherId','HitterId','PitcherSide','HitterSide','PrimaryEvent','PitcherTeamId','HitterTeamId','PA','AB','H','DOUBLE','TRIPLE','HR','TB','BB','SF','HBP'],axis=1)
df_OBP = df.drop(['PitcherId','HitterId','PitcherSide','HitterSide','PrimaryEvent','PitcherTeamId','HitterTeamId','AB','H','DOUBLE','TRIPLE','HR','TB','BB','SF','HBP'],axis=1)
df_OBP['GameId'] = df.GameId
df_OBP['PA'] = df.PA
df_OBP['Value']= pd.DataFrame((df.H+df.BB+df.HBP)/(df.AB+df.BB+df.HBP+df.SF))
df_OBP['Stat'] = 'OBP'
df_OBP.columns = ['GameID',"PA","Value","Stat"]
df_OBP.round(3)

df_SLG = df.filter(["GameId",'PitcherId','HitterId','PitcherSide','HitterSide','PrimaryEvent','PitcherTeamId','HitterTeamId','PA','AB','H','DOUBLE','TRIPLE','HR','TB','BB','SF','HBP'],axis=1)
df_SLG = df.drop(['PitcherId','HitterId','PitcherSide','HitterSide','PrimaryEvent','PitcherTeamId','HitterTeamId','AB','H','DOUBLE','TRIPLE','HR','TB','BB','SF','HBP'],axis=1)
df_SLG['GameId'] = df.GameId
df_SLG['PA'] = df.PA
df_SLG['Value']= pd.DataFrame(((df.H)+(df.DOUBLE*2)+(df.TRIPLE*3)+((df.HR)*4))/(df.AB))
df_SLG['Stat'] = 'SLG'
df_SLG.columns = ['GameID',"PA","Value","Stat"]
df_SLG.round(3)

df_OPS = df.filter(["GameId",'PitcherId','HitterId','PitcherSide','HitterSide','PrimaryEvent','PitcherTeamId','HitterTeamId','PA','AB','H','DOUBLE','TRIPLE','HR','TB','BB','SF','HBP'],axis=1)
df_OPS = df.drop(['PitcherId','HitterId','PitcherSide','HitterSide','PrimaryEvent','PitcherTeamId','HitterTeamId','AB','H','DOUBLE','TRIPLE','HR','TB','BB','SF','HBP'],axis=1)
df_OPS['GameId'] = df.GameId
df_OPS['PA'] = df.PA
df_OPS['Value'] = pd.DataFrame((df.AB*(df.H+df.BB+df.HBP)+df.TB*(df.AB+df.BB+df.SF+df.HBP))/(df.AB*(df.AB+df.BB+df.SF+df.HBP)))
df_OPS['Stat'] = 'OPS'
df_OPS.columns = ['GameID',"PA","Value","Stat"]
df_OPS.round(3)

df_PitchID = df.filter(["GameId",'PitcherId','HitterId','PitcherSide','HitterSide','PrimaryEvent','PitcherTeamId','HitterTeamId','PA','AB','H','DOUBLE','TRIPLE','HR','TB','BB','SF','HBP'],axis=1)
df_PitchID = df.drop(['HitterId','HitterSide','PrimaryEvent','PitcherTeamId','HitterTeamId','PA','AB','H','DOUBLE','TRIPLE','HR','TB','BB','SF','HBP'],axis=1)
df_PitchID['PitcherSide'] = df_PitchID['PitcherSide'].map({'R': 'RHP', 'L': 'LHP'})
df_PitchID['Subject'] = 'PitchID'
df_PitchID.columns = ["GameID","SubjectID","Split","Subject"]
df_PitchID

df_HitterID = df.filter(["GameId",'PitcherId','HitterId','PitcherSide','HitterSide','PrimaryEvent','PitcherTeamId','HitterTeamId','PA','AB','H','DOUBLE','TRIPLE','HR','TB','BB','SF','HBP'],axis=1)
df_HitterID = df.drop(['PitcherId','PitcherSide','PrimaryEvent','PitcherTeamId','HitterTeamId','PA','AB','H','DOUBLE','TRIPLE','HR','TB','BB','SF','HBP'],axis=1)
df_HitterID['HitterSide'] = df_HitterID['HitterSide'].map({'R': 'RHH', 'L': 'LHH'})
df_HitterID['Subject'] = 'HitterID'
df_HitterID.columns = ["GameID","SubjectID","Split","Subject"]
df_HitterID


df_PitchTeamID = df.filter(["GameId",'PitcherId','HitterId','PitcherSide','HitterSide','PrimaryEvent','PitcherTeamId','HitterTeamId','PA','AB','H','DOUBLE','TRIPLE','HR','TB','BB','SF','HBP'],axis=1)
df_PitchTeamID = df.drop(['PitcherId','HitterId','HitterSide','PrimaryEvent','HitterTeamId','PA','AB','H','DOUBLE','TRIPLE','HR','TB','BB','SF','HBP'],axis=1)
df_PitchTeamID['PitcherSide'] = df_PitchTeamID['PitcherSide'].map({'R': 'RHP', 'L': 'LHP'})
df_PitchTeamID['Subject'] = 'PitchTeamID'
df_PitchTeamID.columns = ["GameID","SubjectID","Split","Subject"]
df_PitchTeamID



df_HitterTeamID = df.filter(["GameId",'PitcherId','HitterId','PitcherSide','HitterSide','PrimaryEvent','PitcherTeamId','HitterTeamId','PA','AB','H','DOUBLE','TRIPLE','HR','TB','BB','SF','HBP'],axis=1)
df_HitterTeamID = df.drop(['PitcherId','HitterId','PitcherSide','PrimaryEvent','PitcherTeamId','PA','AB','H','DOUBLE','TRIPLE','HR','TB','BB','SF','HBP'],axis=1)
df_HitterTeamID['HitterSide'] = df_HitterTeamID['HitterSide'].map({'R': 'RHH', 'L': 'LHH'})
df_HitterTeamID['Subject'] = 'HitterTeamID'
df_HitterTeamID.columns = ["GameID","SubjectID","Split","Subject"]
df_HitterTeamID

df_1 = pd.concat([df_PitchID, df_HitterID, df_PitchTeamID, df_HitterTeamID])
df_2 = pd.concat([df_Avg,df_OBP,df_SLG,df_OPS])
df_Final = pd.merge(df_1,df_2,on='GameID')
df_Final.groupby(["SubjectID","Split","Subject","Stat"]).sum().groupby(["Value"]).agg('mean')
df_Final.round(3)




df_Output = df_Final.filter(["SubjectID","Split","Subject","Stat","Value"], axis=1)
df_Output.sort(["SubjectID","Split","Subject","Stat"])
df_Output.to_csv("E:/InsideEdge/Repository/python_hiring_test/python_hiring_test/data/processed/output.csv")













